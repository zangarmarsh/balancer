<?php
use PHPUnit\Framework\TestCase;
use Zangarmarsh\BucketBalancer\Orchestrator;

final class BalancingTest extends TestCase {
    public function testInsertElementsIntoOneBucket(): void {
        $orchestrator = Orchestrator::getInstance();

        $bucket = $orchestrator->createBucket(60, 'A');
        $orchestrator->createBucket(300, 'C');

        $orchestrator->fill([$bucket->getName()], array_fill(0, 50, rand(1, 100)));

        $this->assertCount(50, $bucket->getItems());
    }

    public function testInsertElementsWhenOneBucketIsFullAndCannotRearrange(): void {
        $orchestrator = Orchestrator::getInstance();

        $bucketA = $orchestrator->createBucket(20, 'A');
        $bucketB = $orchestrator->createBucket(10, 'B');

        $orchestrator->fill([$bucketB->getName()], array_fill(0, 10, rand(1,100)));
        $this->assertEquals(0, $bucketB->getResidualCapacity());

        $orchestrator->fill([$bucketB->getName(), $bucketA->getName()], array_fill(0, 10, rand(1,100)));
        $this->assertCount(10, $bucketA->getItems());
    }

    public function testWillBalanceEquallyWithoutExceedingLimits(): void {
        $orchestrator = Orchestrator::getInstance();
        $items = array_fill(0, 10, rand(1, 100));
        
        $bucketA = $orchestrator->createBucket(100, 'A');
        $bucketB = $orchestrator->createBucket(50, 'B');
        $bucketC = $orchestrator->createBucket(10, 'C');

        $orchestrator->fill([$bucketB->getName(), $bucketC->getName()], $items);

        $this->assertCount(8, $bucketB->getItems());
        $this->assertCount(2, $bucketC->getItems());

        $this->assertCount(
            count($bucketB->getItems()) + count($bucketC->getItems()),
            $items
        );
    }

    public function testSequentialInsertWithoutExceedingLimits(): void {
        $orchestrator = Orchestrator::getInstance();

        $bucketA = $orchestrator->createBucket(100, 'A');
        $bucketB = $orchestrator->createBucket(50, 'B');
        $bucketC = $orchestrator->createBucket(10, 'C');

        $orchestrator->fill([$bucketA->getName()], array_fill(0, 50, rand(1,100)));
        $this->assertCount(50, $bucketA->getItems());

        $orchestrator->fill([$bucketB->getName()], array_fill(0, 40, rand(1,100)));
        $this->assertCount(40, $bucketB->getItems());

        $orchestrator->fill([$bucketA->getName(), $bucketB->getName()], array_fill(0, 10, rand(1,100)));
        $this->assertCount(58, $bucketA->getItems());
        $this->assertCount(42, $bucketB->getItems());
    }

    public function testWillReturnFalseWhenBucketIsFull(): void {
        $orchestrator = Orchestrator::getInstance();

        $bucketA = $orchestrator->createBucket(10, 'A');
        $this->assertFalse($orchestrator->fill([$bucketA->getName()], array_fill(0, 11, rand(1,100))));
    }

    public function testRearrangesItemsOnFullBucket(): void {
        $orchestrator = Orchestrator::getInstance();

        $bucketA = $orchestrator->createBucket(100, 'A');
        $bucketB = $orchestrator->createBucket(50, 'B');
        $bucketC = $orchestrator->createBucket(10, 'C');

        $orchestrator->fill([$bucketB->getName(), $bucketC->getName()], array_fill(0, 10, rand(1, 100)));
        $this->assertTrue($orchestrator->fill([$bucketC->getName()], array_fill(0, 10, rand(1,100))));

        $this->assertCount(10, $bucketB->getItems());
        $this->assertCount(10, $bucketC->getItems());
    }

    public function testRearrangesMultipleTimes(): void {
        $orchestrator = Orchestrator::getInstance();

        $bucketA = $orchestrator->createBucket(100, 'A');
        $bucketB = $orchestrator->createBucket(50, 'B');
        $bucketC = $orchestrator->createBucket(10, 'C');

        
        $orchestrator->fill([$bucketB->getName(), $bucketC->getName()], array_fill(0, 10, rand(1, 100)));
        $this->assertCount(8, $bucketB->getItems());
        $this->assertCount(2, $bucketC->getItems());

        $orchestrator->fill([$bucketC->getName()], array_fill(0, 8, rand(1, 100)));
        $this->assertCount(10, $bucketC->getItems());

        $this->assertTrue($orchestrator->fill([$bucketC->getName()], [150]));
        
        $this->assertCount(9, $bucketB->getItems());
        $this->assertCount(10, $bucketC->getItems());

        $orchestrator->fill([$bucketC->getName()], [78]);

        $this->assertCount(10, $bucketB->getItems());
        $this->assertCount(10, $bucketC->getItems());
    }
}