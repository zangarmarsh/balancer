# Balancer

This minimal library will manage load balancing between "buckets" based on virtual weights.

## Prerequisites
- PHP 7.4
- Composer (to install PHPUnit)

## Getting started
Install:

```$ git clone https://akeeron@bitbucket.org/akeeron/balancer.git```

```$ cd balancer```

```$ composer install```

Run the tests:

```$ phpunit --testdox tests/```


## Use
You will find in index.php a sample of usage.