<?php
namespace Zangarmarsh\BucketBalancer;

class Orchestrator {
    protected static $istance;

    /**
     * @var Bucket[]
     */
    protected $buckets = [];
    protected $actions = [];
    protected $totalCapacity = 0;

    final protected function __construct() {}
    final protected function __wakeup() {}
    final protected function __clone() {}

    public static function getInstance() {
        if (!isset($instance)) {
            self::$istance = new static();
        }

        return self::$istance;
    }


    /**
     * Creates a new instance of Bucket and push it into the orchestrator's buckets collection.
     * Also, the weights will be resetted.
     *
     * 
     * @param string $name  The attached bucket's ID
     * @param integer $capacity
     * @return void
     */
    public function createBucket(int $capacity, string $name = null): Bucket {
        if (!$name)
            $name = $this->generateBucketUniqueId();

        $bucket = new Bucket($name, $capacity);

        $this->buckets[$name] = $bucket;
        $this->totalCapacity += $capacity;

        $this->resetWeights();

        return $bucket;
    }


    /**
     *
     * @param array $bucketsIds
     * @param array $elements
     * @return boolean
     */
    public function fill(array $bucketsIds, array $elements, ?bool $listView = false): bool {
        $elementsCount = count($elements);

        if (!$this->totalCapacity || !count($bucketsIds) || !$elementsCount) {
            return false;
        }

        $involvedBucketsInfo = $this->getInvolvedBucketsInfo($bucketsIds);

        if (!count($involvedBucketsInfo))
            return false;

        /**
         * If there isn't enough space to store new items recursively we can try to free up some space
         * moving elements FROM the "currently involved buckets" TO other buckets.
         **/
        if ($elementsCount > $involvedBucketsInfo['residualCapacity']) {
            
            /**
             * Filter relevant actions to avoid a "maximum nested limit reached" error while recursively rearraging items.
             * 
             * Relevant means that the specific action contains at least one occurrence bpertinenteetween the current involved buckets and
             * also in that very action there's some uninvolved bucket.
             * 
             */
            $relevantActions = array_filter(
                $this->actions,
                function($action) use ($bucketsIds) {
                    $involvedBuckets = array_keys($action);

                    return !empty(array_intersect($involvedBuckets, $bucketsIds)) &&
                        !empty(array_diff($involvedBuckets, $bucketsIds));
                }
            );

            if (!$this->rearrangeItems(
                $bucketsIds,
                $elementsCount - $involvedBucketsInfo['residualCapacity'],
                $relevantActions,
                count($relevantActions))) {
                return false;
            }

            /*
            * If items have been rearranged we must recalculate weights and residual capacities
            */
            $this->resetWeights();
            $involvedBucketsInfo = $this->getInvolvedBucketsInfo($bucketsIds);
        }

        $newAction = [];

        foreach ($bucketsIds as $bucketId) {
            $allocableSlotsInThisBucket = $this->getAllocableSlots(
                $this->buckets[$bucketId],
                $involvedBucketsInfo["weights"],
                $elementsCount
            );

            /**
             * Every action is an associative array that has the bucket id as key and the allocable number of slots as value.
             */
            $newAction[$bucketId] =  $allocableSlotsInThisBucket;

            $this->buckets[$bucketId]->addItems(
                array_splice($elements, 0, $allocableSlotsInThisBucket)
            );
        }

        $this->actions[] = $newAction;
        $this->totalCapacity -= $elementsCount;
        $this->resetWeights();

        if ($listView) {
            echo $this->list();
        }

        return true;
    }


    /**
     * Rearranges recursively items to free up some slots in order to store new items.
     * Actions are passed by reference and they will be updated as soon as some elements are moved between two or more buckets.
     *
     * @param array $ids
     * @param integer $itemsThatNeedToBeRearranged
     * @param array $actions
     * @return void
     */
    protected function rearrangeItems(array $ids, int $itemsThatNeedToBeRearranged, array &$actions, int $attempts) {
        if (--$attempts < 0)
            return false;

        $involvedBuckets = array_intersect(array_keys($actions[$attempts]), $ids);
        $uninvolvedBuckets = array_diff(array_keys($actions[$attempts]), $involvedBuckets);

        $rearrangedItems = 0; 
        
        /**
         * Keep looping until the condition $rearrangedItems >= $itemsThatNeedToBeRearranged is equal to true.
         * The first for statement keeps looping until the current "uninvolved" bucket has been filled, then $k variable is incremented and
         * the focus of the loop switches on the next bucket.
         * 
         * The idea behind this nested loop is that N "involved" buckets eagerly move elements into "uninvolved" buckets until
         * they have enough room to get the new items.
         * 
         */
        for ($k = 0, $len = count($uninvolvedBuckets); $this->buckets[$uninvolvedBuckets[$k]]->getResidualCapacity() || ++$k < $len;) {
            foreach ($involvedBuckets as $involvedBucket) {
                if (!$actions[$attempts][$involvedBucket])
                    continue;

                $movedItems = $this->buckets[$uninvolvedBuckets[$k]]->addItems(
                    $this->buckets[$involvedBucket]->splice(
                        0,
                        $actions[$attempts][$involvedBucket] < $itemsThatNeedToBeRearranged ?
                        $actions[$attempts][$involvedBucket] : $itemsThatNeedToBeRearranged
                    )
                );

                $rearrangedItems += $movedItems;
                $actions[$attempts][$involvedBucket] -= $movedItems;
                $actions[$attempts][$uninvolvedBuckets[$k]] += $movedItems;

                if ($rearrangedItems >= $itemsThatNeedToBeRearranged) {
                    return true;
                }
            }
        }

        return $this->rearrangeItems($ids, $itemsThatNeedToBeRearranged - $rearrangedItems, $actions, --$attempts);
    }


    /**
     *
     * Gather a set of useful information to calculate allocable items into the given buckets.
     * 
     * @param Bucket[] $buckets
     * @return integer
     */
    protected function getInvolvedBucketsInfo(array $bucketsIds): array {
        $weights = 0;
        $residualCapacity = 0;

        foreach ($bucketsIds as $bucketId) {
            if (!isset($this->buckets[$bucketId])) {
                printf("There's no bucket with id %s", $bucketId);
                return [];
            }

            $weights += $this->buckets[$bucketId]->getWeight();
            $residualCapacity += $this->buckets[$bucketId]->getResidualCapacity();
        }

        return [
            "weights" => $weights,
            "residualCapacity" => $residualCapacity
        ];
    }


    /**
     * This method computes how many items should be allocated into the several buckets
     * balanced by their updated weights.
     * 
     * The original formula behind this calculus is the following:
     * (BucketBaseWeight * 100 / InvolvedBucketSummedWeights) * howManyItemsImInserting / 100
     * 
     * Which simplified becomes:
     * BucketBaseWeight / InvolvedBucketsSummedWeights * howManyItemsImInserting
     *
     * @param Bucket $bucket
     * @return integer
     */
    protected function getAllocableSlots(Bucket $bucket, $involvedBucketsSummedWeights, $itemsCount): int {
        if (!$involvedBucketsSummedWeights)
            return 0;
        else
            return round($bucket->getWeight() / $involvedBucketsSummedWeights * $itemsCount, 0, PHP_ROUND_HALF_EVEN);
    }


    protected function resetWeights() {
        foreach ($this->buckets as $bucket) {
            $bucket->setWeight(
                $bucket->getResidualCapacity() * 100 / $this->totalCapacity
            );
        }
    }


    /**
     * Returns a string which explains the actual buckets capacities
     *
     * @return string
     */
    public function list(): string {
        $text = '';

        foreach ($this->buckets as $bucket) {
            $text .= sprintf("%s: %d/%d" . PHP_EOL,
                $bucket->getName(),
                count($bucket->getItems()),
                $bucket->getCapacity()
            );
        }

        return $text;
    }


    /**
     * Method used as fallback if the bucket I'm creating as no given name.
     *
     * @return string
     */
    protected function generateBucketUniqueId(): string {
        return md5(uniqid(rand(), true));
    }
}
