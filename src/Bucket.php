<?php
    namespace Zangarmarsh\BucketBalancer;

    class Bucket {
        protected int $capacity;
        protected string $name;
        protected float $weight;
        protected array $items;

        public function __construct(string $name, int $capacity)
        {
            $this->items = [];
            $this->name = $name;
            $this->capacity = $capacity;
        }

        public function getCapacity(): int {
            return $this->capacity;
        }

        public function setCapacity(int $capacity) {
            $this->capacity = $capacity;
        }

        public function getName(): string {
            return $this->name;
        }

        public function setWeight(float $weight) {
            $this->weight = $weight;
        }

        public function getWeight(): float {
            return $this->weight;
        }

        public function getResidualCapacity(): int {
            return $this->capacity - count($this->items);
        }

        /**
         * Returns how many items have been added
         *
         * @param array $items
         * @return integer
         */
        public function addItems(array $items): int {
            for($i = 0, $len = count($items); $i < $len && $this->getResidualCapacity(); ++$i) {
                $this->items[] = $items[$i];
            }

            return $i;
        }

        public function getItems(): array {
            return $this->items;
        }

        public function splice(int $offset, int $length): array {
            return array_splice(
                $this->items,
                $offset,
                $length
            );
        }
    }