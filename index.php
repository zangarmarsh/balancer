<?php

namespace Zangarmarsh\BucketBalancer;

require('vendor/autoload.php');

$orchestrator = Orchestrator::getInstance();

$bucketA = $orchestrator->createBucket(100, 'A');
$orchestrator->fill([$bucketB, $bucketC], array_fill(0, 10, 0), true);